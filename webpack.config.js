// Import plugin to handle relative paths.
const path = require('path');

// Import plugin to split CSS into files.
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Import plugins to remove unnecessary momentjs data.
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

// Import a plugin that lets us copy over static assets.
const CopyPlugin = require('copy-webpack-plugin');

//
const staticAssets =
{
	patterns:
	[
		{ from: "source/data/", to: "data/" },
		{ from: "source/images/", to: "images/" },
	],
};

module.exports =
{
	// Add your application's scripts below
	entry:
	{
		index:
		[
			'./source/index.html',
			'./source/index.ts',
			'./source/index.css'
		],
	},
	mode: 'development',
	devtool: 'inline-source-map',
	output:
	{
		path: path.resolve(__dirname, 'public/'),

		filename: '[name].js',
		clean: true,
	},
	module:
	{
		rules:
		[
			{
				// Only run `.ts` files through the the webpack and plugins
				test: /\.ts$/,
				use: 'ts-loader',

				// Do not parse node modules.
				exclude:
				[
					/node_modules/
				],

				// But do include source maps for node modules.
				// enforce: 'pre',
				// use: ['source-map-loader'],
			},
			{
				test: /\.css$/,
				use:
				[
					MiniCssExtractPlugin.loader,
					"css-loader",
				],
			},
			{
				test: /\.(png|jpg|jpeg|gif)$/i,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
			{
				test: /\.webmanifest$/,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
			{
				// ...
				test: /\.html$/i,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
			{
				test: /\.html$/i,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
		]
	},
	plugins:
	[
		new MomentLocalesPlugin({ localesToKeep: ['en'] }),
		new MiniCssExtractPlugin({ filename: '[name].css' }),
		new CopyPlugin(staticAssets),
	],
	resolve:
	{
	        extensions: ['.js', '.jsx', '.ts', '.json'],
		fallback:
		{
			// Antlr requires dependencies:
			assert: false,

			// Price oracle dependencies:
			path: false,
			os: false,
			fs: false,

			// Anyhedge requires dependencies:
			events: require.resolve("events/"),
			net: false,
			tls: false,

			// BitcoinRpcNetworkProvider requires dependencies:
			https: false,
			http: false,
			util: false,
			url: false,
		}
	},
}
