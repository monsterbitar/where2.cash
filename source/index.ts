// Enable use of moment to handle time.
import moment from 'moment';

import OpeningHours from 'opening_hours';

// Import support for storing data in IndexedDB
import { get, set, entries } from 'idb-keyval';

// ...
import { getAreasFromPosition, getPointsFromArea } from './script/openstreetmap';

// Import data managers.
import { MapManager } from './script/map';
import { PointManager } from './script/points';
import { AreaManager, LEVEL_NATION, LEVEL_PROVINCE } from './script/areas';

// ..
import type { OverpassAreaWithTimestamp } from './script/areas';
import type { OverpassNode, OverpassArea, OverpassPointGeom } from "overpass-ts";

// ...
import type { Communities, CommunityMember } from './script/community';

class Where2Cash
{
	// Setup data managers.
	private readonly map = MapManager;
	private readonly areas = AreaManager;
	private readonly points = PointManager;

	// Used to store a list of areas available in the current view.
	private viewAreas: Array<number>;

	// Used to store the viewed areas region and nation names.
	private viewRegion: number;
	private viewNation: number;

	// Set default state for map features.
	private location: boolean = true;
	private tracking: boolean = false;
	private unsupported: boolean = false;
	private unverified: boolean = false;

	//
	private communities: Communities;
	private merchants: Map<number, any> = new Map();

	// TODO: Remove this entirely.
	public peopleControl: any;
	public markers: any;
	public layers: any;

	constructor()
	{
		this.initialize();
	}

	public async initialize(): Promise<void>
	{
		// Load cached data into memory.
		// NOTE: Ignoring areas to trigger update on load.
		// this.areas.load();
		this.points.load();

		// Set initial view, previous and device position.
		// this.viewPosition = currentPosition;
		// this.prevPosition = currentPosition;
		// this.devicePosition = currentPosition;

		this.map.initialize();

		if(this.location)
		{
			this.map.enableLocation();
		}

		// TODO: Ensure that map shows the last used position on start.

		// Fetch the preparsed merchant data from local storage.
		const storedMerchants = await get('merchants');

		// If there was no merchants already parsed..
		if(!storedMerchants || !(storedMerchants instanceof Map))
		{
			// Load the base set of merchants from file.
			await this.loadMerchants();
		}
		else
		{
			this.merchants = storedMerchants;
		}

		// Always load communities data.
		await this.loadCommunities();

		// Show the local communities early when possible.
		await this.updateLocalCommunities();

		// Populate the map once.
		await this.populateMap();

		// Manually query the current map for new information.
		await this.queryMapAreas(this.map.viewPosition.latitude, this.map.viewPosition.longitude);

		// When the user drags the map, check if we have entered a new area.
		this.map.events.on('changedViewLocation', this.detectAreaChange.bind(this));
	}

	async loadMerchants(): Promise<void>
	{
		// Request the merchant data as a GET request.
		const response = await fetch('data/vendors.json');

		// Parse the data as a JSON file.
		const osmData = await response.json();

		// Loop over the data elements and..
		for(const index in osmData.elements)
		{
			// ..add the merchants to the entries by their id numbers.
			this.merchants.set(osmData.elements[index].id, osmData.elements[index]);
		}

		// Store the data to local storage.
		await set('merchants', this.merchants);
	}

	async loadCommunities(): Promise<void>
	{
		// Fetch the communities using a get request.
		// TODO: Make this data dynamic and managed by a directory service.
		const response = await fetch('data/communities.json');

		// Parse the returned data as a JSON file.
		this.communities = await response.json();
	}

	async detectAreaChange(): Promise<void>
	{
		// Query the new position for region information.
		await this.queryMapAreas(this.map.viewPosition.latitude, this.map.viewPosition.longitude);
	}

	async queryMapAreas(latitude: number, longitude: number): Promise<void>
	{
		// Avoid querying invalid positions.
		if((longitude < -180) || (longitude > 180))
		{
			return;
		}

		// FIXME: move the age check out of the subfunction, or name things better.
		// This checks if we haven't updated the area in a while, and updates if necessery.
		await this.updateLocalArea(latitude, longitude);
		await this.updateLocalVendors();
		await this.updateLocalCommunities();
	}

	async updateLocalArea(latitude: number, longitude: number): Promise<void>
	{
		const queryResult = await getAreasFromPosition(latitude, longitude);

		// Extract the returned areas and cast type for clarity.
		const areas = queryResult.elements as Array<OverpassArea>;

		//
		let nationId: number = 0;
		let regionId: number = 0;

		//
		let currentLevel = 0;

		// Clear the list of areas for the current view:
		this.viewAreas = [];

		// Parse the list of areas relating to the current position.
		for(const area of areas)
		{
			// Ignore areas that are not administrative.
			if(!area.tags || area.tags['boundary'] !== 'administrative')
			{
				continue;
			}

			// Fetch existing timestamp, if available.
			const timestamp = (await this.areas.timestamp(area.id) ?? '2000-01-01T00:00:01.000Z');

			// Store the area with a timestamp.
			this.areas.set({ ...area, ...{ timestamp }})

			// Add this area to the currently viewed areas.
			this.viewAreas.push(area.id);

			// Parse the admin level into a number.
			const adminLevel = Number(area.tags['admin_level']);

			// Use the english name, when possible.
			// TODO: Make this locale aware in the future.
			const areaName = (area.tags['name:en'] ?? area.tags['name']);

			// Store this area as the currently viewed nation.
			if(adminLevel === LEVEL_NATION)
			{
				nationId = area.id;
			}

			// Store this area as the currently viewed region.
			// NOTE: Target area to relate to the smallest that is still a province or larger.
			if((adminLevel > currentLevel) && (adminLevel <= LEVEL_PROVINCE))
			{
				// Update the current best level.
				currentLevel = adminLevel;

				// Set this area as the view region.
				regionId = area.id;
			}
		}

		// If we were unable to find the nation of the current position, ignore the data.
		if(!nationId)
		{
			console.warn(`Could not determine nation while inspecting current coordinate (${latitude}, ${longitude})`);

			return;
		}

		// Save the updated areas to local storage, in the background.
		this.areas.save();

		// Update the current views nation and best area references.
		this.viewNation = nationId;
		this.viewRegion = regionId;
	}

	async updateLocalVendors(): Promise<void>
	{
		// Fetch the current local area.
		const localArea = await this.areas.get(this.viewRegion);

		// Skip vendor update if we currently don't know about any local areas.
		if(!localArea)
		{
			return;
		}

		// TODO: Use the timestamp for the vendor data, rather than the area.
		const dataAgeInHours = moment().diff(moment(localArea.timestamp), 'hours', false);

		// If it has been more than an hour since we updated the data set for this area..
		if((dataAgeInHours >= 1))
		{
			// Show notification.
			// TODO: Consider re-adding nation name in this text.
			// TODO: Make a utility function to parse names from tags.
			this.map.notifications.info('Updating', `Updating map data for ${(localArea.tags['name:en'] ?? localArea.tags['name'])}`);

			// Request updated points of interests for this area.
			const queryResult = await getPointsFromArea(localArea.id, localArea.timestamp);

			// Extract the timestamp for legibility.
			const timestamp = queryResult.osm3s.timestamp_osm_base;

			// Extract the returned points and cast type for clarity.
			const points = queryResult.elements as Array<OverpassNode>;

			// Update points
			for(const point of points)
			{
				// TODO: If the point has been deleted..
				if(false)
				{
					// Delete related markers etc.
					this.map.removeMarker(point.id);

					// Delete the point
					this.points.remove(point.id);
				}
				else
				{
					// Store the point with a timestamp.
					this.points.set({ ...point, ...{ timestamp }})

					// Update related markers etc:
					// Construct the popup content for this marker.
					const popup = await this.buildMarkerPopup(point.lat, point.lon, point.tags!);

					// Select which layer and icon to use for this marker.
					const { layer, icon } = await this.chooseMarkerLayerAndIcon(point.tags!);

					// Add the marker to the map.
					this.map.setMarker(point.id, point.lat, point.lon, layer, icon, popup);
				}
			}

			// Show notification.
			this.map.notifications.success('Updated', `Updated ${points.length} entries.`);

			// Update timestamp for this area.
			this.areas.set({ ...localArea, ...{ timestamp }});

			// Save the updated points to local storage, in the background.
			this.points.save();
		}
	}

	async updateLocalCommunities(): Promise<void>
	{
		const localCommunities: Map<number, any> = new Map();

		const communityIcons =
		{
			event: "<i class='far fa-clock'></i>",
			group: "<i class='fas fa-users'></i>",
			support: "<i class='fas fa-user-graduate'></i>",
			business: "<i class='fas fa-user-tie'></i>",
		};

		if(!this.viewRegion)
		{
			return;
		}

		// Iterate over each area in our currently viewed region..
		for(const localAreaId of this.viewAreas)
		{
			// Ignore this area if there are no communities in it.
			if(!this.communities[localAreaId])
			{
				continue;
			}

			// Add this area if not already added.
			if(!localCommunities.has(localAreaId))
			{
				const localCommunity =
				{
					area: await this.areas.get(localAreaId),
					members: this.communities[localAreaId],
				};

				localCommunities.set(localAreaId, localCommunity);
			}
		}

		// Remove the control if it already exist.
		if(this.peopleControl)
		{
			this.peopleControl.remove();
			delete this.peopleControl;
		}

		// Add the control if there is ambassadors to display.
		if(localCommunities.size > 0)
		{
			let displayList = '';

			for(const { area, members } of localCommunities.values())
			{
				displayList += `<b>${(area.tags['name:en'] ?? area.tags['name'])}</b>`;
				displayList += "<ul>";

				for(const member of members)
				{
					// Only show engagement that is timeless or in the future.
					if(!member.timestamp || (member.timestamp && moment().isBefore(moment.unix(member.timestamp))))
					{
						displayList += "<li>";

						// @ts-ignore
						displayList += communityIcons[member.type];
						displayList += "<section>";
						displayList += `<b>${member.name}</b>`;
						displayList += "<hr />";

						if(member.timestamp)
						{
							const eventDate = moment.unix(member.timestamp).format("dddd, MMMM Do, hh:mm");

							displayList += `<small><i>${eventDate}</i></small>`;
						}

						if(member.phone) { displayList += "<small><a href='callto:" + member.phone + "'><i class='fas fa-phone'></i> " + member.phone + "</a></small>"; }
						if(member.email) { displayList += "<small><a href='mailto:" + member.email + "'><i class='far fa-envelope'></i> " + member.email + "</a></small>"; }
						if(member.website) { displayList += "<small><a href='" + member.website + "'><i class='fas fa-globe-americas'></i> " + member.website + "</a></small>"; }

						if(typeof member['contact:signal'] !== 'undefined') { displayList += "<small><a href='" + member['contact:signal'] + "'><i class='fas fa-pencil-alt'></i> Chat on signal</a></small>"; }
						if(typeof member['contact:facebook'] !== 'undefined') { displayList += "<small><a href='" + member['contact:facebook'] + "'><i class='far fa-facebook'></i> " + member['contact:facebook'] + "</a></small>"; }
						if(typeof member['contact:twitter'] !== 'undefined') { displayList += "<small><a href='" + member['contact:twitter'] + "'><i class='far fa-twitter'></i> " + member['contact:twitter'] + "</a></small>"; }

						if(member.description)
						{
							displayList += `<p>${member.description}</p>`;
						}

						displayList += "</section>";
						displayList += "</li>";
					}
				}
				displayList += "</ul>";

				// @ts-ignore
				this.peopleControl = L.control.custom(
				{
					position: 'bottomright',
					content: '<section class="header"><i class="fas fa-street-view"></i><span>Local community</span></section><section class="content">' + displayList + "</section>",
					classes: 'leaflet-custom-control community minimized',
					events:
					{
						click: function(data: any)
						{
							// @ts-ignore
							if(this.className.includes('minimized'))
							{
								// @ts-ignore
								this.className = this.className.replace('minimized', 'maximized');
							}
							else
							{
								// @ts-ignore
								this.className = this.className.replace('maximized', 'minimized');
							}
						},
					}
				});

				this.peopleControl.addTo(this.map.map);
			}
		}

		console.log('Local communities:', localCommunities);
		console.log('Local regions', this.viewAreas);
	}

	async buildMarkerLabel(tags: any): Promise<string>
	{
		let label;

		// Figure out which icon and description name to use for this marker.
		const groupName = (tags.tourism ? "tourism" : tags.amenity ? "amenity" : tags.shop ? "shop" : ".");
		const groupType = (tags.tourism ? tags.tourism : tags.amenity ? tags.amenity : tags.shop ? tags.shop : "unknown");

		// Capitalize the description and turn underscores into spaces to make a more human readable description.
		const description = (groupType.charAt(0).toUpperCase() + groupType.substring(1)).replace('_', ' ');

		const labelIcon = "<img src='./images/symbols/" + groupName + "/" + groupType + ".svg' />";
		const labelTitle = "<div>" + (tags.name ? "<b>" + tags.name + "</b>" : (tags.operator ? "<b>" + tags.operator + "</b>" : "")) + "<small>" + description + "</small></div>";

		//
		label = "<section class='marker'>" + labelIcon + labelTitle + "</section>";

		const contactTagIcons =
		{
			'phone':			"<i class='fas fa-phone'></i>",
			'contact:phone':	"<i class='fas fa-phone'></i>",
			'email':			"<i class='far fa-envelope'></i>",
			'website':		"<i class='fas fa-globe-americas'></i>",
			'operator:website':	"<i class='fas fa-globe-americas'></i>",
			'facebook':		"<i class='fab fa-facebook'></i>",
			'twitter':		"<i class='fab fa-twitter'></i>",
			'reddit':			"<i class='fab fa-reddit-alien'></i>",
		}

		const socialMedia =
		{
			'facebook': 'facebook.com',
			'twitter': 'twitter.com'
		}

		let fieldExist: any = {};
		let phoneExist: boolean = false;
		let contactFields: Array<string> = [];

		for(const contactIndex in contactTagIcons)
		{
			if(tags[contactIndex])
			{
				// For each social media supported..
				for(let socialIndex in socialMedia)
				{
					// Verify that we have not already added this social media type.
					if(!fieldExist[socialIndex])
					{
						// Check for direct and contact field.
						let tagDirect = (contactIndex == socialIndex ? true : false);
						let tagContact = (contactIndex == ('contact:' + socialIndex) ? true : false)
						let tagWebsite = false;

						// Try to match website with http and https, as well as with and without www prefix.
						// @ts-ignore
						tagWebsite = ((tagWebsite || tags[contactIndex].startsWith('https://' + socialMedia[socialIndex])) ? true : false);
						// @ts-ignore
						tagWebsite = ((tagWebsite || tags[contactIndex].startsWith('http://' + socialMedia[socialIndex])) ? true : false);
						// @ts-ignore
						tagWebsite = ((tagWebsite || tags[contactIndex].startsWith('https://www.' + socialMedia[socialIndex])) ? true : false);
						// @ts-ignore
						tagWebsite = ((tagWebsite || tags[contactIndex].startsWith('http://www.' + socialMedia[socialIndex])) ? true : false);

						// If the social media was detected..
						if(tagDirect || tagContact || tagWebsite)
						{
							// Add this field to the list of contact field.
							// @ts-ignore
							contactFields.push(contactTagIcons[socialIndex] + "&nbsp;&nbsp;<a href='" + tags[contactIndex] + "'>" + tags[contactIndex] + "</a>");

							// Mark this social media as added.
							fieldExist[socialIndex] = true;
						}
					}
				}

				if(contactIndex == 'website')
				{
					let websites = tags[contactIndex].split(",");

					for(let websiteIndex in websites)
					{
						if(!fieldExist['facebook'] && websites[websiteIndex].startsWith('https://www.facebook.com') || websites[websiteIndex].startsWith('http://www.facebook.com'))
						{
							contactFields.push(contactTagIcons['facebook'] + "&nbsp;&nbsp;<a href='" + websites[websiteIndex] + "'>" + websites[websiteIndex] + "</a>");
						}
						else if(!fieldExist['twitter'] && websites[websiteIndex].startsWith('https://twitter.com') || websites[websiteIndex].startsWith('http://twitter.com'))
						{
							contactFields.push(contactTagIcons['twitter'] + "&nbsp;&nbsp;<a href='" + websites[websiteIndex] + "'>" + websites[websiteIndex] + "</a>");
						}
						else if(!fieldExist['reddit'] && websites[websiteIndex].startsWith('https://www.reddit.com') || websites[websiteIndex].startsWith('http://www.reddit.com'))
						{
							contactFields.push(contactTagIcons['reddit'] + "&nbsp;&nbsp;<a href='" + websites[websiteIndex] + "'>" + websites[websiteIndex] + "</a>");
						}
						else
						{
							contactFields.push(contactTagIcons[contactIndex] + "&nbsp;&nbsp;<a href='" + websites[websiteIndex] + "'>" + websites[websiteIndex] + "</a>");
						}
					}
				}
				else if(!phoneExist && (contactIndex === 'phone' || contactIndex === 'contact:phone'))
				{
					phoneExist = true;

					contactFields.push(contactTagIcons[contactIndex] + "&nbsp;&nbsp;<a href='tel:" + tags[contactIndex] + "'>" + tags[contactIndex] + "</a>");
				}
				else if(contactIndex === 'email' || contactIndex === 'contact:email')
				{
					// @ts-ignore
					contactFields.push(contactTagIcons[contactIndex] + "&nbsp;&nbsp;<a href='mailto:" + tags[contactIndex] + "'>" + tags[contactIndex] + "</a>");
				}
				else
				{
					// @ts-ignore
					contactFields.push(contactTagIcons[contactIndex] + "&nbsp;&nbsp;" + tags[contactIndex]);
				}
			}
		}

		if(contactFields.length == 0)
		{
			contactFields.push("<div style='text-align: center; color: rgba(0, 0, 0, 0.44);'>No information available.</div>");
		}

		if(contactFields.length > 0)
		{
			label += "<hr /><span class='markerContactField'>" + contactFields.join("</span><span class='markerContactField'>") + "</span>";
		}

		return label;
	}

	async buildMarkerActions(latitude: number, longitude: number, tags: any): Promise<string>
	{
		let actions = [];

		if((tags["payment:bitcoincash"] == "yes") || (tags["currency:BCH"] == "yes"))
		{
			actions.push("<a><img src='images/bitcoincash_green.png'></i><small>Supported</small></a>");
		}
		else if((tags["payment:bitcoincash"] == "no") || (tags["currency:BCH"] == "no"))
		{
			actions.push("<a><img src='images/bitcoincash_red.png'></i><small>Unsupported</small></a>");
		}
		else
		{
			actions.push("<a><img src='images/bitcoincash_yellow.png'></i><small>Unverified</small></a>");
		}

		// TODO: implement timestamping / activity pulses.
		if(tags['opening_hours'])
		{
			try
			{
				// Attempt to parse the opening hours for this location.
				// NOTE: null is passed in to use default location, for parsing holidays etc.
				const openingHours = new OpeningHours(tags['opening_hours'], null);

				// ...
				const isOpen = openingHours.getState();

				//
				const currentDate = moment();
				const nextChange = moment(openingHours.getNextChange());

				// ..
				if(nextChange.isSame(currentDate, 'day'))
				{
					actions.push(`<a><i class='far fa-clock'></i><small>${(isOpen ? 'Closes' : 'Opens')}&nbsp;${nextChange.format('HH:mm')}</small></a>`);
				}
				else
				{
					actions.push(`<a><i class='far fa-clock'></i><small>${(isOpen ? 'Open' : 'Closed')}</small></a>`);
				}
			}
			catch(error)
			{
				// Do nothing.
			}
		}
		else
		{
			//actions.push("<a><i class='far fa-clock' style='color: grey;'></i><small>Unknown</small></a>");
		}

		actions.push("<a href='https://www.google.com/maps/dir/?api=1&destination=" + latitude + "," + longitude + "'><i class='fas fa-car-side'></i><small>Navigate</small</a>");

		return "<ul class='marker_actions'><li>" + actions.join('</li><li>') + "</li></ul>";
	}

	async buildMarkerPopup(latitude: number, longitude: number, tags: Record<string, any>): Promise<any>
	{
		const label = await this.buildMarkerLabel(tags);
		const actions = await this.buildMarkerActions(latitude, longitude, tags);

		const popup = `${label}<hr />${actions}`;

		return popup;
	}

	async chooseMarkerLayerAndIcon(tags: Record<string, any>)
	{
		// If we're showing the event display..
		if((window.location.hash === '#pizza') && (tags["cuisine"] && tags["cuisine"].toLowerCase().includes('pizza')))
		{
			return { layer: 'events', icon: 'pizza' };
		}

		// Create a marker of a suitable color depending on payment support.
		if((tags["payment:bitcoincash"] === "yes") || (tags["currency:BCH"] === "yes"))
		{
			return { layer: 'supported', icon: 'green' };
		}
		else if((tags["payment:bitcoincash"] === "no") || (tags["currency:BCH"] === "no"))
		{
			return { layer: 'unsupported', icon: 'red' };
		}
		else
		{
			return { layer: 'unverified', icon: 'yellow' };
		}
	}

	async populateMap(): Promise<void>
	{
		// For each merchant..
		for(const merchant of this.merchants.values())
		{
			// Construct the popup content for this marker.
			const popup = await this.buildMarkerPopup(merchant.lat, merchant.lon, merchant.tags);

			// Select which layer and icon to use for this marker.
			const { layer, icon } = await this.chooseMarkerLayerAndIcon(merchant.tags);

			// Add the marker to the map.
			this.map.setMarker(merchant.id, merchant.lat, merchant.lon, layer, icon, popup);
		}
	}
}

// Create a singleton instance of the application.
const application = new Where2Cash();
