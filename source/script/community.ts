// Define a community member.
export interface CommunityMember
{
	// Required that type is from a known list.
	type: "event" | "group" | "support" | "business";

	// Require the both name and description exists.
	name: string;
	description?: string;

	// Allow any other key-value strings.
	[key: string]: string | undefined;
};

// Declare OSM area as a number to clarify relation to OSM.
export type OpenStreetMapArea = number;

// Declare the Bitcoin Cash community to a list of members per area.
export type Communities = Record<OpenStreetMapArea, Array<CommunityMember>>;
