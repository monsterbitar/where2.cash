// Import support for storing data in IndexedDB
import { get, set } from 'idb-keyval';

// ...
import type { OverpassArea } from "overpass-ts";

// ...
export interface OverpassAreaWithTimestamp extends OverpassArea
{
	timestamp: string;
}

// Export administrative boundary levels.
export const LEVEL_NATION = 2;
export const LEVEL_PROVINCE = 4;

/**
 * Utility class to manage collections of area.
 */
export class AreaManager
{
	// Define a cache for area information
	private static areas: Record<number, OverpassAreaWithTimestamp> = {};

	/**
	* Checks if an area exist.
	*
	* @param id the open street map id number for the area
	*
	* @returns true if the area exist, false otherwise.
	*/
	public static async exist(id: number): Promise<boolean>
	{
		return !!this.areas[id];
	}

	public static async timestamp(id: number): Promise<string|undefined>
	{
		return (this.areas[id] ? this.areas[id].timestamp : undefined);
	}

	/**
	* Gets information on a given area.
	*
	* @param id the open street map id number for the area.
	*
	* @return the area information if it exist, or undefined otherwise.
	*/
	public static async get(id: number): Promise<OverpassAreaWithTimestamp | undefined>
	{
		return this.areas[id];
	}

	/**
	* Stores or updates a new area.
	*
	* @param area the open street map area to store or update.
	*/
	public static async set(area: OverpassAreaWithTimestamp): Promise<void>
	{
		this.areas[area.id] = area;
	}

	/**
	* Removes an existing area.
	*
	* @param id the open street map id number to remove
	*
	* @throws an error if the area does not already exist.
	*/
	public static async remove(id: number): Promise<void>
	{
		delete this.areas[id];
	}

	/**
	 * Loads a set of currently known areas from local storage.
	 */
	public static async load(): Promise<void>
	{
		// Fetch any saved areas.
		const savedAreas = await get('areas') as Record<number, OverpassAreaWithTimestamp>;

		// Load the saved areas if they exist, or start with an empty set.
		this.areas = (savedAreas ?? {});
	}

	/**
	 * Stores the currently known areas into local storage.
	 */
	public static async save(): Promise<void>
	{
		await set('areas', this.areas);
	}
}
