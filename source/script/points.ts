// Import support for storing data in IndexedDB
import { get, set } from 'idb-keyval';

//
import type { OverpassNode } from "overpass-ts";

/**
 * Utility class to manage collections of points.
 */
export class PointManager
{
	// Define a cache for point information
	private static points: Record<number, OverpassNode> = {};

	/**
	* Checks if a point of interest exist.
	*
	* @param id the open street map id number for the node.
	*
	* @returns true if the point exist, false otherwise.
	*/
	public static async exist(id: number): Promise<boolean>
	{
		return !!this.points[id];
	}

	/**
	* Gets information on a given point of interest.
	*
	* @param id the open street map id number for the node.
	*
	* @return the point information if it exist, or undefined otherwise.
	*/
	public static async get(id: number): Promise<OverpassNode | undefined>
	{
		return this.points[id];
	}

	/**
	* Stores or updates a new point of interest.
	*
	* @param point the open street map node to store or update.
	*/
	public static async set(point: OverpassNode): Promise<void>
	{
		this.points[point.id] = point;
	}

	/**
	* Removes an existing point of interest.
	*
	* @param id the open street map id number to remove
	*
	* @throws an error if the point does not already exist.
	*/
	public static async remove(id: number): Promise<void>
	{
		delete this.points[id];
	}

	/**
	 * Loads a set of currently known points from local storage.
	 */
	public static async load(): Promise<void>
	{
		// Fetch any saved points.
		const savedPoints = await get('points') as Record<number, OverpassNode>;

		// Load the saved points if they exist, or start with an empty set.
		this.points = (savedPoints ?? {});
	}

	/**
	 * Stores the currently known points into local storage.
	 */
	public static async save(): Promise<void>
	{
		await set('points', this.points);
	}
}
